# Access Control Service

The Access Control Service module manages permissions and roles for users and partners, including creating, updating, retrieving, and deleting permissions and roles. It provides a set of endpoints to interact with these functionalities.

## Endpoint Description

### Health Check

- Endpoint: GET /:version/health
- Description: Checks the health status of the Access Control Service.
- Handler: Health

### Create Permissions

- Endpoint: POST /:version/permissions
- Description: Creates new permissions.
- Handler: CreatePermissions

### Add Partner-Specific Permissions

- Endpoint: POST /:version/partners/:partner_id/permissions
- Description: Adds permissions specific to a partner.
- Handler: AddPartnerSpecificPermissions

### View Partner-Specific Permissions

- Endpoint: GET /:version/partners/:partner_id/permissions
- Description: Retrieves permissions specific to a partner.
- Handler: ViewPartnerSpecificPermissions

### Delete Partner-Specific Permission

- Endpoint: DELETE /:version/partners/:partner_id/permissions/:permission_id
- Description: Deletes a specific permission for a partner.
- Handler: DeletePartnerSpecificPermission

### List Permissions

- Endpoint: GET /:version/permissions
- Description: Retrieves a list of all permissions.
- Handler: ListPermissions

### List Permissions by ID

- Endpoint: GET /:version/permissions/:id
- Description: Retrieves permissions by ID.
- Handler: ListPermissionsByID

### Get Member Permissions

- Endpoint: GET /:version/members/:id/permissions
- Description: Retrieves permissions for a specific member.
- Handler: GetPermissionsMember

### Delete Permission

- Endpoint: DELETE /:version/permissions/:id
- Description: Deletes a specific permission.
- Handler: DeletePermission

### Get All Roles

- Endpoint: GET /:version/roles
- Description: Retrieves a list of all roles.
- Handler: GetAllRoles

### Get Role by ID

- Endpoint: GET /:version/roles/:id
- Description: Retrieves a specific role by ID.
- Handler: GetRolesById

### Delete Role

- Endpoint: DELETE /:version/roles/:id
- Description: Deletes a specific role.
- Handler: DeleteRoles

### Delete Role for Member

- Endpoint: DELETE /:version/members/:id/roles
- Description: Deletes a role for a specific member.
- Handler: DeleteRoleForMember

### Get All Features

- Endpoint: GET /:version/features
- Description: Retrieves a list of all features.
- Handler: GetAllFeatures

### Delete Feature

- Endpoint: DELETE /:version/features/:id
- Description: Deletes a specific feature.
- Handler: DeleteFeatures

### Access Control

- Endpoint: POST /:version/access-control
- Description: Manages access control.
- Handler: AccessControl

## Environment Variables

- ACCESS_CONTROL_DEBUG: Indicates whether debugging is enabled for the Access Control Service. (Example: true)
- ACCESS_CONTROL_PORT: The port number on which the Access Control Service is running. (Example: 8080)
- ACCESS_CONTROL_DB_USER: The username for connecting to the Access Control Service database. (Example: postgres)
- ACCESS_CONTROL_DB_PORT: The port number on which the Access Control Service database is running. (Example: 5433)
- ACCESS_CONTROL_DB_PASSWORD: The password for the database user to connect to the Access Control Service database. (Example: tuneverse)
- ACCESS_CONTROL_DB_DATABASE: The name of the Access Control Service database. (Example: tuneverse_local)
- ACCESS_CONTROL_ACCEPTED_VERSIONS: The accepted versions of the Access Control Service. (Example: v1_0)
- ACCESS_CONTROL_DB_SCHEMA: The schema within the Access Control Service database. (Example: public)
- ACCESS_CONTROL_DB_HOST: The hostname or IP address of the Access Control Service database server. (Example: 10.1.0.229)
- ACCESS_CONTROL_LOCALISATION_SERVICE_URL: The URL for the localization service used by the Access Control Service. (Example: http://10.1.0.206:8025/api/v1)
- ACCESS_CONTROL_CACHE_EXPIRATION: The expiration time (in hours) for the Access Control Service cache. (Example: 1)
- ACCESS_CONTROL_ENDPOINT_URL: The URL for the main endpoint of the Access Control Service. (Example: http://10.1.0.206:8025/api/v1)
- ACCESS_CONTROL_LOGGER_SERVICE_URL: The URL for the logger service used by the Access Control Service. (Example: http://10.1.0.226:90/api/v1.0)
- ACCESS_CONTROL_LOGGER_SECRET: The secret key for authenticating with the logger service. (Example: "logger service secret")
- ACCESS_CONTROL_ERROR_HELP_LINK: A URL linking to a help document for errors related to the Access Control Service. (Example: https://docs.google.com/spreadsheets/d/1dgBRdaj-xVt5DcrBNIEjqjzH5paAmKyKJ4DeihXvcDo/edit?usp=sharing)
- ACCESS_CONTROL_PARTNER_SERVICE_URL: The URL for the partner service used by the Access Control Service. (Example: http://10.1.0.90:8031/api/v1/partners)
