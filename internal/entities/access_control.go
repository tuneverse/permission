package entities

// Permission represents a specific permission entity with its associated properties.
type Permission struct {
	ID        int64  `json:"id"`
	FeatureID int64  `json:"feature_id"`
	Name      string `json:"name"`
	Endpoint  string `json:"endpoint"`
	URL       string `json:"url"`
	Method    string `json:"method"`
}

// MetaData represents metadata associated with a collection of items.
type MetaData struct {
	Total       int64  `json:"total,omitempty"`
	PerPage     int64  `json:"per_page,omitempty"`
	CurrentPage int64  `json:"current_page,omitempty"`
	Next        string `json:"next,omitempty"`
	Previous    string `json:"prev,omitempty"`
}

// Params represents parameters used for pagination or filtering in a request.
type Params struct {
	Page  int32
	Limit int32
}

// Permissions represent the permission ids
type Permissions struct {
	IDs []int `json:"ids"`
}
