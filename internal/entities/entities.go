package entities

import "gitlab.com/tuneverse/toolkit/models"

// EnvConfig represents the configuration structure for the application.
type EnvConfig struct {
	Debug            bool     `default:"true" split_words:"true"`  // Indicates whether the application is in debug mode (default: true)
	Port             int      `default:"8080" split_words:"true"`  // The port on which the server listens (default: 8080)
	Db               Database `split_words:"true"`                 // Database configuration
	AcceptedVersions []string `required:"true" split_words:"true"` // List of accepted API versions (required)
	LoggerSecret     string   `split_words:"true"`                 // Secret key for logging
}

// Database represents the configuration for the database connection.
type Database struct {
	Driver    string // Database driver
	User      string // Database username
	Password  string // Database password
	Port      int    // Database port
	Host      string // Database host
	DATABASE  string // Database name
	Schema    string // Database schema
	MaxActive int    // Maximum number of active connections
	MaxIdle   int    // Maximum number of idle connections
}

// SuccessResponse represents a successful response with structured information.
type SuccessResponse struct {
	MetaData *models.MetaData `json:"meta_data,omitempty"`
	Data     interface{}      `json:"data,omitempty"`
}
