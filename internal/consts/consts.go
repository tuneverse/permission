package consts

import "os"

// AppName stores the Application name
const (
	AppName      = "access_control"
	DatabaseType = "postgres"
)

// Accepted versions
const (
	AcceptedVersions = "v1"
)

// Context Configuration
const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
	ContextEndPoints              = "context-endpoints"
)

// Context setting values
const (
	ContextErrorResponses        = "context-error-response"
	ContextLocallizationLanguage = "lan"
)

// headers
const (
	HeaderLocallizationLanguage = "Accept-Language"
)

// Cache Configuration
const (
	CacheErrorData = "CACHE_ERROR_DATA"
	ExpiryTime     = 180
)

// KeyNames
const (
	ValidationErr             = "validation_error"
	ForbiddenErr              = "forbidden"
	UnauthorisedErr           = "unauthorized"
	NotFound                  = "not found"
	InternalServerErr         = "internal_server_error"
	Errors                    = "errors"
	AllError                  = "AllError"
	Registration              = "registration"
	ErrorCode                 = "errorCode"
	MemberIDErr               = "member_id"
	PostBillingAddress        = "post_billing_address"
	CommonMessage             = "common_message"
	PartnerNotFound           = "partner_not_found"
	PartnerPermissionNotFound = "partner_permission_not_found"
	PermissionNotFound        = "permission_not_found"
	ParseErr                  = "Error occured while parsing "
	InvalidLimit              = "invalid_limit"
	InvalidPage               = "invalid_page"
	PartnerID                 = "partner_id"
	UUID                      = "uuid"
	PermissionDeleted         = "permission_deletion_failed"
)

// Constants representing predefined messages for different scenarios in the application.
const (
	EndpointErr = "Error occured while loading endpoints from service"
	ContextErr  = "Error occured while loading error from service"
	Success     = "Successfully deleted the partner specific permission"
	BindErr     = "Error occured while binding"
)

// pagination
const (
	DefaultPage  = 1
	DefaultLimit = 10
)

// PartnerPermissonView success message
const PartnerPermissonView = "partner permisssions retreved successfully"

var (
	LocalisationServiceURL = os.Getenv("ACCESS_CONTROL_LOCALISATION_SERVICE_URL")
	LoggerServiceURL       = os.Getenv("ACCESS_CONTROL_LOGGER_SERVICE_URL")
	EndpointURL            = os.Getenv("ACCESS_CONTROL_ENDPOINT_URL")
)
