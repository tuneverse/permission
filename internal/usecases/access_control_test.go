package usecases_test

import (
	"context"
	"errors"
	"fmt"
	"permission/internal/consts"
	"permission/internal/entities"
	"permission/internal/repo/mock"
	"permission/internal/usecases"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
)

func TestAddPartnerSpecificPermissions(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	clientOpt := &logger.ClientOptions{
		Service:  consts.AppName,
		LogLevel: "trace",
	}

	logger.InitLogger(clientOpt)

	// Mock repository
	mockRepo := mock.NewMockAccessControlRepoImply(ctrl)

	// Create access control use case with the mock repository
	accessControlUseCases := usecases.NewAccessControlUseCases(mockRepo)

	// Test cases
	t.Run("Add Permissions Successfully", func(t *testing.T) {
		partnerID := "96b05633-69d7-4144-b0f9-73765b9ea7ca"
		permissionIDs := entities.Permissions{IDs: []int{1, 2, 3}}

		// Mock repository behavior
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), partnerID).Return(true, nil)
		mockRepo.EXPECT().AddPartnerSpecificPermissions(gomock.Any(), partnerID, permissionIDs.IDs).Return(nil)

		// Call the function
		fieldsMap, err := accessControlUseCases.AddPartnerSpecificPermissions(context.Background(), partnerID, permissionIDs)

		// Assertions
		require.NoError(t, err, "Expected no error")
		require.Empty(t, fieldsMap, "Expected empty fieldsMap")
	})

	t.Run("Partner Not Found", func(t *testing.T) {
		partnerID := "614608f2-6538-4733-aded-96f902007255"
		permissionIDs := entities.Permissions{IDs: []int{1, 2, 3}}

		// Mock repository behavior
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), partnerID).Return(false, nil)

		// Call the function
		fieldsMap, err := accessControlUseCases.AddPartnerSpecificPermissions(context.Background(), partnerID, permissionIDs)

		// Assertions
		require.NoError(t, err, "Expected no error")
		require.Contains(t, fieldsMap, "common_message", "Expected 'common_message' in fieldsMap")
	})

	t.Run("Error Adding Permissions", func(t *testing.T) {
		partnerID := "96b05633-69d7-4144-b0f9-73765b9ea7ca"
		permissionIDs := entities.Permissions{IDs: []int{1, 2, 3}}

		// Mock repository behavior
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), partnerID).Return(true, nil)
		mockRepo.EXPECT().AddPartnerSpecificPermissions(gomock.Any(), partnerID, permissionIDs.IDs).
			Return(errors.New("permission addition error"))

		// Call the function
		fieldsMap, err := accessControlUseCases.AddPartnerSpecificPermissions(context.Background(), partnerID, permissionIDs)

		// Assertions
		require.Error(t, err, "Expected an error")
		require.Empty(t, fieldsMap, "Expected empty fieldsMap")
	})
}

func TestViewPartnerSpecificPermissions(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	clientOpt := &logger.ClientOptions{
		Service:  consts.AppName,
		LogLevel: "trace",
	}

	logger.InitLogger(clientOpt)

	mockRepo := mock.NewMockAccessControlRepoImply(ctrl)

	accessContolUseCases := usecases.NewAccessControlUseCases(mockRepo)

	// Define test input parameters
	ctx := context.Background()

	partnerID := "302e1dca-2183-4750-b50a-fc3e64a198e1"

	t.Run("Success case", func(t *testing.T) {
		params := entities.Params{
			Page:  1,
			Limit: 10,
		}

		expectedData := []entities.Permission{
			{
				ID:        1,
				FeatureID: 1,
				Name:      "Permission 1",
				Endpoint:  "/endpoint1",
				URL:       "http://example.com/1",
				Method:    "GET",
			},
			{
				ID:        2,
				FeatureID: 2,
				Name:      "Permission 2",
				Endpoint:  "/endpoint2",
				URL:       "http://example.com/2",
				Method:    "POST",
			},
			{
				ID:        3,
				FeatureID: 3,
				Name:      "Permission 3",
				Endpoint:  "/endpoint3",
				URL:       "http://example.com/3",
				Method:    "PUT",
			},
			{
				ID:        10,
				FeatureID: 1,
				Name:      "Sample1",
				Endpoint:  "/endpoint1",
				URL:       "http://example.com/1",
				Method:    "GET",
			},
			{
				ID:        11,
				FeatureID: 2,
				Name:      "test",
				Endpoint:  "/endpoint2",
				URL:       "http://example.com/2",
				Method:    "POST",
			},
			{
				ID:        12,
				FeatureID: 3,
				Name:      "testPermission",
				Endpoint:  "/endpoint3",
				URL:       "http://example.com/3",
				Method:    "PUT",
			},
		}

		expectedMetaData := &models.MetaData{
			Total:       6,
			PerPage:     10,
			CurrentPage: 1,
			Next:        0,
		}

		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), partnerID).Return(true, nil)
		mockRepo.EXPECT().
			ViewPartnerSpecificPermissions(gomock.Any(), partnerID, gomock.Any(), gomock.Any()).
			Return(expectedData, int64(len(expectedData)), nil)

		// Call the method being tested
		permission, metaData, fileldsmap, err := accessContolUseCases.ViewPartnerSpecificPermissions(ctx, partnerID, params)

		require.Equal(t, expectedData, permission)
		require.Equal(t, expectedMetaData, metaData)

		require.Empty(t, fileldsmap, "Expected empty fieldsMap")

		require.Nil(t, err)

	})

	t.Run("PartnerNotFound", func(t *testing.T) {
		params := entities.Params{
			Page:  1,
			Limit: 10,
		}

		// Define expected results
		expectedMetaData := &models.MetaData{}
		expectedMetaData = nil

		// Set up expectations for the mock repository
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), partnerID).Return(false, nil)

		permission, metaData, fieldsMap, err := accessContolUseCases.ViewPartnerSpecificPermissions(ctx, partnerID, params)

		if len(permission) != 0 {
			t.Errorf("Expected an empty slice slice, but got %v", permission)
		}
		require.Equal(t, expectedMetaData, metaData)

		// Assert the result (should contain an error message for artist not found)
		require.Contains(t, fieldsMap, "common_message", "Expected 'common_message' in fieldsMap")

		require.Nil(t, err)
	})

	t.Run("ErrorFromRepository", func(t *testing.T) {
		params := entities.Params{
			Page:  1,
			Limit: 10,
		}

		// Set up expectations for the mock repository to return an error.
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), partnerID).Return(true, nil)
		mockRepo.EXPECT().
			ViewPartnerSpecificPermissions(gomock.Any(), partnerID, gomock.Any(), gomock.Any()).
			Return(nil, int64(0), fmt.Errorf("error from repository"))

		_, _, fieldsMap, err := accessContolUseCases.ViewPartnerSpecificPermissions(ctx, partnerID, params)

		if len(fieldsMap) != 0 {
			t.Errorf("Expected no validation errors, but got %v", fieldsMap)
		}

		require.NotNil(t, err)
	})
}

func TestDeletePartnerSpecificPermission(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	clientOpt := &logger.ClientOptions{
		Service:  consts.AppName,
		LogLevel: "trace",
	}

	logger.InitLogger(clientOpt)

	mockRepo := mock.NewMockAccessControlRepoImply(ctrl)

	accessContolUseCases := usecases.NewAccessControlUseCases(mockRepo)

	// Define test input parameters
	ctx := context.Background()

	partnerID := "302e1dca-2183-4750-b50a-fc3e64a198e1"

	t.Run("Success Case", func(t *testing.T) {
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPermissionExcists(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPartnerPermissionExist(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().DeletePartnerSpecificPermission(gomock.Any(), gomock.Any(), gomock.Any()).Return(1, nil)

		permissionId := "5"
		fieldsMap, err := accessContolUseCases.DeletePartnerSpecificPermission(ctx, partnerID, permissionId)

		if len(fieldsMap) != 0 {
			t.Errorf("Expected no validation errors, but got %v", fieldsMap)
		}

		require.Nil(t, err)
	})

	t.Run("Partner Not Found", func(t *testing.T) {
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), gomock.Any()).Return(false, nil)

		permissionId := "5"
		fieldsMap, err := accessContolUseCases.DeletePartnerSpecificPermission(ctx, partnerID, permissionId)

		if _, ok := fieldsMap["common_message"]; !ok {
			t.Errorf("Expected 'common_message' in result, but got %v", fieldsMap)
		}

		require.Nil(t, err)
	})

	t.Run("No record found", func(t *testing.T) {
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPermissionExcists(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPartnerPermissionExist(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().DeletePartnerSpecificPermission(gomock.Any(), gomock.Any(), gomock.Any()).Return(0, nil)

		permissionId := "5"
		fieldsMap, err := accessContolUseCases.DeletePartnerSpecificPermission(ctx, partnerID, permissionId)

		if _, ok := fieldsMap["common_message"]; !ok {
			t.Errorf("Expected 'common_message' in result, but got %v", fieldsMap)
		}

		require.Nil(t, err)
	})

	t.Run("Internal Server Error", func(t *testing.T) {
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPermissionExcists(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPartnerPermissionExist(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().DeletePartnerSpecificPermission(gomock.Any(), gomock.Any(), gomock.Any()).Return(0, fmt.Errorf("Error from repo"))

		permissionId := "5"
		fieldsMap, err := accessContolUseCases.DeletePartnerSpecificPermission(ctx, partnerID, permissionId)

		if len(fieldsMap) != 0 {
			t.Errorf("Expected no validation errors, but got %v", fieldsMap)
		}

		require.NotNil(t, err)
	})

	t.Run("Permission Exists", func(t *testing.T) {
		mockRepo.EXPECT().IsPartnerExist(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPermissionExcists(gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().IsPartnerPermissionExist(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().DeletePartnerSpecificPermission(gomock.Any(), gomock.Any(), gomock.Any()).Return(1, nil)

		permissionID := "5"
		fieldsMap, err := accessContolUseCases.DeletePartnerSpecificPermission(ctx, partnerID, permissionID)

		if len(fieldsMap) != 0 {
			t.Errorf("Expected no validation errors, but got %v", fieldsMap)
		}

		require.Nil(t, err)
	})

}
