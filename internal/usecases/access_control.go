package usecases

import (
	"context"
	"permission/internal/consts"
	"permission/internal/entities"
	"permission/internal/repo"

	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// AccessControlUseCases struct represents the use cases for the example entity.
type AccessControlUseCases struct {
	repo repo.AccessControlRepoImply
}

// AccessControlUseCaseImply is an interface that defines the methods for handling access control
type AccessControlUseCaseImply interface {
	AddPartnerSpecificPermissions(context.Context, string, entities.Permissions) (map[string][]string, error)
	ViewPartnerSpecificPermissions(context.Context, string, entities.Params) ([]entities.Permission, *models.MetaData, map[string][]string, error)
	DeletePartnerSpecificPermission(context.Context, string, string) (map[string][]string, error)
}

// NewAccessControlUseCases creates a new instance of AccessControlUseCases.
func NewAccessControlUseCases(exampleRepo repo.AccessControlRepoImply) AccessControlUseCaseImply {
	return &AccessControlUseCases{
		repo: exampleRepo,
	}
}

// AddPartnerSpecificPermissions adds specific permissions to the given partner.
// It first checks if the partner exists, and if so, calls the repository method
// to insert the provided permission IDs for the partner.
//
// Params:
//   - ctx: The context.Context for the operation.
//   - partnerID: The unique identifier of the partner to whom permissions will be added.
//   - permissionIDs: An instance of entities.Permissions containing the permission IDs to be added.
//
// Returns:
//   - fieldsMap: A map containing validation error messages, if any.
//   - error: An error, if any, encountered during the operation.
func (accessControl *AccessControlUseCases) AddPartnerSpecificPermissions(ctx context.Context, partnerID string, permissionIDs entities.Permissions) (map[string][]string, error) {
	fieldsMap := map[string][]string{}
	log := logger.Log().WithContext(ctx)
	_, err := uuid.Parse(partnerID)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, consts.PartnerID, consts.UUID)
		return fieldsMap, err
	}

	isPartnerExists, err := accessControl.repo.IsPartnerExist(ctx, partnerID)

	if err != nil {
		log.Errorf("AddPartnerSpecificPermissions failed, validation error, err = %s", err.Error())
		return nil, err
	}

	if !isPartnerExists {
		log.Errorf("AddPartnerSpecificPermissions failed, validation error, err = partner does not exists.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PartnerNotFound)
		return fieldsMap, nil
	}

	err = accessControl.repo.AddPartnerSpecificPermissions(ctx, partnerID, permissionIDs.IDs)

	if err != nil {
		return nil, err
	}

	return nil, nil
}

// ViewPartnerSpecificPermissions is a use case function that handles the retrieval of specific permissions
// associated with a partner from the repository. It validates the partner's existence, handles pagination
// parameters, and delegates the retrieval to the repository. If successful, it returns the retrieved permissions,
// pagination metadata, and an empty validation fields map.
//
// Params:
//
//	@ctx: The context for executing the function.
//	@partnerID: The unique identifier of the partner for whom permissions are to be retrieved.
//	@params: A structure containing optional pagination parameters (limit and page).
//
// Returns:
//   - A slice of entities.Permission representing the retrieved permissions.
//   - An instance of entities.MetaData containing pagination metadata (total count of permissions).
//   - An empty validation fields map (map[string][]string) indicating no validation errors.
//   - An error if any occurred during the validation or repository operation.
func (accessControl *AccessControlUseCases) ViewPartnerSpecificPermissions(ctx context.Context, partnerID string, params entities.Params) (
	[]entities.Permission, *models.MetaData, map[string][]string, error) {

	fieldsMap := map[string][]string{}
	log := logger.Log().WithContext(ctx)
	_, err := uuid.Parse(partnerID)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, consts.PartnerID, consts.UUID)
		return []entities.Permission{}, nil, fieldsMap, nil
	}

	isPartnerExists, err := accessControl.repo.IsPartnerExist(ctx, partnerID)

	if err != nil {
		log.Errorf("ViewPartnerSpecificPermissions failed, validation error, err = %s", err.Error())
		return []entities.Permission{}, nil, nil, err
	}

	if !isPartnerExists {
		log.Errorf("ViewPartnerSpecificPermissions failed, validation error, err = partner does not exists.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PartnerNotFound)
		return []entities.Permission{}, nil, fieldsMap, nil
	}

	page, limit := utils.Paginate(params.Page, params.Limit, int32(consts.DefaultLimit))

	offset := (page - 1) * limit

	permissions, total, err := accessControl.repo.ViewPartnerSpecificPermissions(ctx, partnerID, int64(offset), int64(limit))

	if err != nil {
		return []entities.Permission{}, nil, fieldsMap, err
	}

	metaData := &models.MetaData{
		Total:       total,
		PerPage:     limit,
		CurrentPage: page,
	}

	metaData = utils.MetaDataInfo(metaData)

	return permissions, metaData, nil, nil
}

// DeletePartnerSpecificPermission is a use case function that handles the deletion of a specific
// permission associated with a partner. It performs validation checks, including partner existence
// and permission existence, and delegates the deletion operation to the repository layer.
//
// Params:
//
//	@ctx: The context for executing the use case operation.
//	@partnerID: The unique identifier of the partner.
//	@permissionID: The unique identifier of the permission to be deleted.
//
// Returns:
//   - A map of validation error fields if any validation errors occur, otherwise an empty map.
//   - An error if any occurred during the use case operation.
func (accessControl *AccessControlUseCases) DeletePartnerSpecificPermission(ctx context.Context, partnerID, permissionID string) (map[string][]string, error) {
	fieldsMap := map[string][]string{}
	log := logger.Log().WithContext(ctx)

	_, err := uuid.Parse(partnerID)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, consts.PartnerID, consts.UUID)
		return fieldsMap, err
	}

	isPartnerExists, err := accessControl.repo.IsPartnerExist(ctx, partnerID)

	if err != nil {
		log.Errorf("DeletePartnerSpecificPermission failed, validation error, err = %s", err.Error())
		return nil, err
	}

	if !isPartnerExists {
		log.Errorf("DeletePartnerSpecificPermission failed, validation error, err = partner does not exists.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PartnerNotFound)
		return fieldsMap, nil
	}
	isPermissionExists, err := accessControl.repo.IsPermissionExcists(ctx, permissionID)
	if err != nil {
		log.Errorf("DeletePartnerSpecificPermission failed,not valid permission id, validation error, err = %s", err.Error())
		return nil, err
	}

	if !isPermissionExists {
		log.Errorf("DeletePartnerSpecificPermission failed invalid permission id, validation error, err = partner does not exists.")
		return fieldsMap, nil
	}

	isPermissionForPartnerExists, err := accessControl.repo.IsPartnerPermissionExist(ctx, partnerID, permissionID)
	if err != nil {
		log.Errorf("DeletePartnerSpecificPermission failed due to no partner permission excistance err = %s", err.Error())
	}
	hasPermissionInAccessRolePermission, err := accessControl.repo.HasAccessRolePermissionId(ctx, permissionID)
	if err != nil {
		log.Errorf(" Hard delete DeletePartnerSpecificPermission failed due to error in fetching permission id  excistance in access_role_permission err = %s", err.Error())
	}
	if !hasPermissionInAccessRolePermission && !isPermissionForPartnerExists {
		rowsEffectedDeletion, err := accessControl.repo.DeletePermission(ctx, permissionID)
		if err != nil {
			log.Errorf(" Hard delete DeletePartnerSpecificPermission failed due to error in hard deleting permission err = %s", err.Error())
		}
		if rowsEffectedDeletion == 0 {
			log.Errorf(" Hard DeletePartnerSpecificPermission failed err = permission does not exists.")
			utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PermissionNotFound)
			return fieldsMap, nil
		}
		return nil, nil
	}

	rowsEffected, err := accessControl.repo.DeletePartnerSpecificPermission(ctx, partnerID, permissionID)

	if err != nil {
		log.Errorf("DeletePartnerSpecificPermission failed, validation error, err = %s", err.Error())
		return nil, err
	}

	if rowsEffected == 0 {
		log.Errorf("DeletePartnerSpecificPermission failed, validation error, err = permission does not exists.")
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.PermissionNotFound)
		return fieldsMap, nil
	}

	return nil, nil
}
