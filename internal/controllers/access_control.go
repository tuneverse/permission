package controllers

import (
	"net/http"
	"permission/internal/consts"
	"permission/internal/entities"
	"permission/internal/usecases"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/models/api"
	"gitlab.com/tuneverse/toolkit/utils"
)

// AccessControlController is a struct that manages the HTTP routing and interaction
// with the use cases for handling examples.
type AccessControlController struct {
	router   *gin.RouterGroup
	useCases usecases.AccessControlUseCaseImply
}

// NewAccessControlController creates a new instance of MemberController.
func NewAccessControlController(router *gin.RouterGroup, memberUseCase usecases.AccessControlUseCaseImply) *AccessControlController {
	return &AccessControlController{
		router:   router,
		useCases: memberUseCase,
	}
}

// InitRoutes initializes the routes for the ExampleController.
func (accessControl *AccessControlController) InitRoutes() {

	accessControl.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, accessControl, "Health")
	})

	accessControl.router.POST("/:version/partners/:partner_id/permissions", func(ctx *gin.Context) {
		version.RenderHandler(ctx, accessControl, "AddPartnerSpecificPermissions")
	})

	accessControl.router.GET("/:version/partners/:partner_id/permissions", func(ctx *gin.Context) {
		version.RenderHandler(ctx, accessControl, "ViewPartnerSpecificPermissions")
	})
	accessControl.router.DELETE("/:version/partners/:partner_id/permissions/:permission_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, accessControl, "DeletePartnerSpecificPermission")
	})

}

// Health handles the health endpoint.
func (accessControl *AccessControlController) Health(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

// AddPartnerSpecificPermissions handles the endpoint for adding specific permissions to a partner.
// It validates the request payload, checks the endpoint existence, and adds permissions to the partner.
// The function returns appropriate HTTP responses based on the success or failure of the operation.
//
// Params:
//
//	@ctx: The Gin context for handling the request.
//
// HTTP Request:
//   - Method: POST
//   - URL Path: /:version/partners/:partner_id/permissions
//
// Path Parameters:
//   - partner_id: The unique identifier for the partner to whom permissions will be added.
func (accessControl *AccessControlController) AddPartnerSpecificPermissions(ctx *gin.Context) {
	var (
		ids          entities.Permissions
		responseData = api.Response{
			Message: consts.ValidationErr,
			Code:    http.StatusBadRequest,
			Errors:  struct{}{},
			Data:    struct{}{},
			Status:  "failure",
		}
	)
	partnerID := ctx.Param("partner_id")

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	if !isEndpointExists {
		log.Errorf("Add Partner Specific Permissions failed, validation error, err = endpoint name is not existing in the db.")
		responseData.Message = consts.EndpointErr
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	contextError, contextStatusError := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !contextStatusError {
		log.Errorf("Add Partner Specific Permissions failed")
		responseData.Message = consts.ContextErr
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	if err := ctx.Bind(&ids); err != nil {
		log.Errorf("Add Partner Specific Permissions failed, binding error, err = %s", err.Error())
		responseData.Errors = err.Error()
		responseData.Message = consts.BindErr
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	fieldsMap, err := accessControl.useCases.AddPartnerSpecificPermissions(ctx, partnerID, ids)

	if len(fieldsMap) != 0 {
		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("Add Partner Specific Permissions failed, validation error, err = %s	", fields)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)

		if !valStatus {
			log.Errorf("Add Partner Specific Permissions failed")
			responseData.Message = consts.ParseErr
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}

		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	if err != nil {
		log.Errorf("Add Partner Specific Permissions failed, internal server error, err = %s", err.Error())
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")

		if !valStatus {
			log.Errorf("Add Partner Specific Permissions failed")
			responseData.Message = consts.ParseErr
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}
		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	log.Info("Add Partner Specific Permissions added successfully.")

	responseData.Code = http.StatusCreated
	responseData.Message = "partner specific permissions added successfully"
	responseData.Status = "success"
	ctx.JSON(http.StatusCreated, responseData)
}

// ViewPartnerSpecificPermissions is a handler function for retrieving specific permissions
// associated with a partner identified by the provided partner ID. It handles HTTP GET
// requests to the endpoint /:version/partners/:partner_id/permissions. The function
// supports pagination using query parameters "limit" (maximum number of permissions per page)
// and "page" (page number for pagination).
//
// Params:
//
//	@ctx: The Gin context for handling the request.
//
// HTTP Request:
//   - Method: GET
//   - URL Path: /:version/partners/:partner_id/permissions
//
// Path Parameters:
//   - partner_id: The unique identifier for the partner whose permissions are to be retrieved.
//
// Query Parameters:
//   - limit (optional): The maximum number of permissions to retrieve per page (default: 10).
//   - page (optional): The page number for pagination (default: 1).
func (accessControl *AccessControlController) ViewPartnerSpecificPermissions(ctx *gin.Context) {
	var (
		permissions  []entities.Permission
		metaData     *models.MetaData
		err          error
		params       entities.Params
		responseData = api.Response{
			Message: consts.ValidationErr,
			Code:    http.StatusBadRequest,
			Errors:  struct{}{},
			Data:    struct{}{},
			Status:  "failure",
		}
	)
	ctxt := ctx.Request.Context()
	fieldsMap := map[string][]string{}
	log := logger.Log().WithContext(ctxt)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	if !isEndpointExists {
		log.Errorf("ViewPartnerSpecificPermissions failed, validation error, err = endpoint name is not existing in the db.")
		responseData.Message = consts.EndpointErr
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	contextError, contextStatusError := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !contextStatusError {
		log.Errorf("ViewPartnerSpecificPermissions failed")
		responseData.Message = consts.ContextErr
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	// query parameters
	limitStr := ctx.Query("limit")
	pageStr := ctx.Query("page")

	limit, err := strconv.Atoi(limitStr)

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("ViewPartnerSpecificPermissions failed, invalid limt, err = %s", err.Error())
		fields := utils.FieldMapping(fieldsMap)
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.InvalidLimit)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("ViewPartnerSpecificPermissions failed")
			responseData.Message = consts.ParseErr
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}

		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	page, err := strconv.Atoi(pageStr)

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("ViewPartnerSpecificPermissions failed, invalid page, err = %s", err.Error())
		fields := utils.FieldMapping(fieldsMap)
		utils.AppendValuesToMap(fieldsMap, consts.CommonMessage, consts.InvalidLimit)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			log.Errorf("ViewPartnerSpecificPermissions failed")
			responseData.Message = consts.ParseErr
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}
		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	params.Limit = int32(limit)
	params.Page = int32(page)

	partnerID := ctx.Param("partner_id")

	permissions, metaData, fieldsMap, err = accessControl.useCases.ViewPartnerSpecificPermissions(ctxt, partnerID, params)

	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("ViewPartnerSpecificPermissions failed, validation error, err = %s", fields)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if !valStatus {
			logger.Log().WithContext(ctxt).Errorf("ViewPartnerSpecificPermissions failed")
			responseData.Message = consts.ParseErr
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}
		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	if err != nil {
		log.Errorf("ViewPartnerSpecificPermissions failed, internal server error, err = %s", err.Error())
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if !valStatus {
			logger.Log().WithContext(ctxt).Errorf("ViewPartnerSpecificPermissions failed")
			responseData.Message = consts.ParseErr
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}
		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	log.Info("Partner permissions retrieved successfully")
	data := entities.SuccessResponse{
		MetaData: metaData,
		Data:     permissions,
	}
	responseData.Code = http.StatusOK
	responseData.Status = "success"
	responseData.Data = data
	responseData.Message = "partner permissions retrieved successfully"
	ctx.JSON(http.StatusOK, responseData)

}

// DeletePartnerSpecificPermission is a handler function for deleting a specific permission
// associated with a partner. It extracts partner ID and permission ID from the request context,
// performs validation checks, and delegates the delete operation to the use case layer.
// If successful, it responds with a success message. If any validation or internal server errors
// occur, appropriate error responses are sent back to the client.
//
// Params:
//
//	@ctx: The Gin context for handling the request.
//
// HTTP Request:
//   - Method: DELETE
//   - URL Path: /:version/partners/:partner_id/permissions/:permission_id
//
// URL Parameters:
//   - partner_id: The unique identifier of the partner associated with the permission.
//   - permission_id: The unique identifier of the permission to be deleted.
func (accessControl *AccessControlController) DeletePartnerSpecificPermission(ctx *gin.Context) {
	var responseData = api.Response{
		Message: consts.ValidationErr,
		Code:    http.StatusBadRequest,
		Errors:  struct{}{},
		Data:    struct{}{},
		Status:  "failure",
	}

	partnerID := ctx.Param("partner_id")
	permissionID := ctx.Param("permission_id")

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	if !isEndpointExists {
		log.Errorf("DeletePartnerSpecificPermission failed, validation error, err = endpoint name is not existing in the db.")
		responseData.Message = consts.ValidationErr
		responseData.Code = http.StatusBadRequest
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	contextError, contextStatusError := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !contextStatusError {
		responseData.Message = consts.ContextErr
		responseData.Code = http.StatusBadRequest
		log.Errorf("DeletePartnerSpecificPermission failed")
		ctx.JSON(http.StatusBadRequest, responseData)
		return
	}

	fieldsMap, err := accessControl.useCases.DeletePartnerSpecificPermission(ctx, partnerID, permissionID)

	if len(fieldsMap) != 0 {
		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("DeletePartnerSpecificPermission failed, validation error, err = %s	", fields)
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)

		if !valStatus {
			log.Errorf("DeletePartnerSpecificPermission failed")
			responseData.Message = consts.ParseErr
			responseData.Code = http.StatusBadRequest
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}

		responseData.Message = consts.ParseErr + err.Error()
		responseData.Code = int(errorCode)
		responseData.Errors = val
		ctx.JSON(int(errorCode), responseData)
		return
	}

	if err != nil {
		log.Errorf("DeletePartnerSpecificPermission failed, internal server error, err = %s", err.Error())
		val, valStatus, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")

		if !valStatus {
			log.Errorf("DeletePartnerSpecificPermission failed")
			responseData.Message = consts.ParseErr + err.Error()
			responseData.Code = http.StatusInternalServerError
			ctx.JSON(http.StatusBadRequest, responseData)
			return
		}

		responseData.Message = "invalid permission id"
		responseData.Errors = val
		responseData.Code = http.StatusBadRequest
		ctx.JSON(int(errorCode), responseData)
		return
	}

	// Respond with success message if the delete is successful
	responseData.Code = http.StatusOK
	responseData.Status = "success"
	responseData.Message = "deleted partner specific permission successfully"
	ctx.JSON(http.StatusOK, responseData)
}
