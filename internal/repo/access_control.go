package repo

import (
	"context"
	"database/sql"
	"fmt"
	"permission/internal/entities"
	"strings"
)

// AccessControlRepo represents a repository for handling access control related data in the database.
type AccessControlRepo struct {
	db *sql.DB
}

// AccessControlRepoImply is an interface specifying the methods for the example repository.
type AccessControlRepoImply interface {
	AddPartnerSpecificPermissions(context.Context, string, []int) error
	IsPartnerExist(context.Context, string) (bool, error)
	ViewPartnerSpecificPermissions(context.Context, string, int64, int64) ([]entities.Permission, int64, error)
	DeletePartnerSpecificPermission(context.Context, string, string) (int, error)
	IsPartnerPermissionExist(ctx context.Context, partnerID, permissionID string) (bool, error)
	HasAccessRolePermissionId(ctx context.Context, permissionID string) (bool, error)
	DeletePermission(ctx context.Context, permissionID string) (int, error)
	IsPermissionExcists(ctx context.Context, permissionID string) (bool, error)
}

// NewAccessControlRepo creates a new instance of AccessControlRepo.
func NewAccessControlRepo(db *sql.DB) AccessControlRepoImply {
	return &AccessControlRepo{db: db}
}

// IsPartnerExist checks if a partner with the specified ID exists in the database.
//
// This method queries the database to count the number of partners with the given ID.
//
// Parameters:
//
//	@ctx (context.Context): The context for the database operation.
//	@id (string): The ID of the partner to check for existence.
//
// Returns:
//
//	int: The count of partners with the specified ID (0 or 1).
//	error: Any potential error that occurred during the database query.
func (accessControl *AccessControlRepo) IsPartnerExist(ctx context.Context, id string) (bool, error) {
	var exists bool

	query := `SELECT EXISTS(SELECT 1 FROM public.partner WHERE id = $1)`
	err := accessControl.db.QueryRow(query, id).Scan(&exists)
	if err != nil {
		return false, err
	}

	return exists, nil
}

// AddPartnerSpecificPermissions inserts multiple permission IDs associated with a partner into the database.
// It constructs a dynamic SQL statement for batch insert and executes it to add permissions for the given partner.
//
// Params:
//   - ctx: The context.Context for the database transaction.
//   - partnerID: The unique identifier of the partner to whom permissions will be added.
//   - permissionIDs: A slice containing permission IDs to be added for the partner.
//
// Returns:
//   - error: An error, if any, encountered during the database operation.
func (accessControl *AccessControlRepo) AddPartnerSpecificPermissions(ctx context.Context, partnerID string, permissionIDs []int) error {
	// Prepare the SQL statement for inserting partner permissions
	sqlStatement := "INSERT INTO public.partner_permission(partner_id, permission_id) VALUES "
	var values []interface{}

	i := 0

	// Build the values for the SQL statement dynamically
	for _, permissionID := range permissionIDs {
		sqlStatement += "($%d, $%d),"
		sqlStatement = fmt.Sprintf(sqlStatement, i+1, i+2)
		values = append(values, partnerID, permissionID)
		i += 2
	}

	// Remove the trailing comma and execute the SQL statement
	sqlStatement = strings.TrimSuffix(sqlStatement, ",")
	_, err := accessControl.db.ExecContext(ctx, sqlStatement, values...)
	if err != nil {
		return err
	}

	return nil
}

// ViewPartnerSpecificPermissions retrieves specific permissions associated with a partner
// from the database based on the provided partner ID. It accepts a database context, the
// partner's ID, an offset value for pagination, and a limit specifying the maximum number
// of permissions to retrieve per request.
//
// Params:
//
//	@ctx: The database context for executing the query.
//	@id: The unique identifier of the partner for whom permissions are to be retrieved.
//	@offset: The offset for pagination, specifying the starting position of the query results.
//	@limit: The maximum number of permissions to retrieve per page.
//
// Returns:
//   - A slice of entities.Permission representing the retrieved permissions.
//   - An integer representing the total count of permissions associated with the partner.
//   - An error if any occurred during the database query.
func (accessControl *AccessControlRepo) ViewPartnerSpecificPermissions(ctx context.Context, id string, offset, limit int64) ([]entities.Permission, int64, error) {
	permission := entities.Permission{}
	permissionList := []entities.Permission{}
	totalCount := 0

	query := `SELECT permission.id, 
				feature_id, 
				name, 
				endpoint, 
				COALESCE(url, ''), 
				method,
				COUNT(*) OVER () AS total_count 
				FROM 
					public.permission 
				LEFT JOIN 
					public.partner_permission on permission.id = partner_permission.permission_id 
				WHERE partner_id = $1 ORDER BY name OFFSET $2 LIMIT $3;`

	row, err := accessControl.db.QueryContext(ctx, query, id, offset, limit)

	if err != nil {
		return []entities.Permission{}, 0, err
	}

	for row.Next() {
		err := row.Scan(
			&permission.ID,
			&permission.FeatureID,
			&permission.Name,
			&permission.Endpoint,
			&permission.URL,
			&permission.Method,
			&totalCount,
		)

		if err != nil {
			return []entities.Permission{}, 0, err
		}

		permissionList = append(permissionList, permission)
	}

	return permissionList, int64(totalCount), nil
}

// DeletePartnerSpecificPermission is a repository function that deletes a specific permission
// associated with a partner from the database. It takes the partner ID and permission ID as
// parameters and executes a DELETE query on the partner_permission table. It returns the number
// of affected rows (permissions deleted) and any error that occurred during the deletion process.
//
// Params:
//
//	@ctx: The context for executing the database query.
//	@partnerID: The unique identifier of the partner.
//	@permissionID: The unique identifier of the permission to be deleted.
//
// Returns:
//   - The number of affected rows (permissions deleted).
//   - An error if any occurred during the deletion operation.
func (accessControl *AccessControlRepo) DeletePartnerSpecificPermission(ctx context.Context, partnerID, permissionID string) (int, error) {
	query := `
	UPDATE public.permission
	SET is_deleted = 'true'
	WHERE id=$1;
			`
	result, err := accessControl.db.ExecContext(ctx, query, permissionID)
	if err != nil {
		return 0, err
	}

	// Check the affected rows
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(rowsAffected), nil
}

// IsPartnerPermissionExist checks if a partner permission with the specified ID exists in the database.
//
// This method queries the database to count the number of partners with the given ID.
//
// Parameters:
//
//	@ctx (context.Context): The context for the database operation.
//	@id (string): The ID of the partner and it's permission to check for existence.
//
// Returns:
//
//	int: The count of partners with the specified ID (0 or 1).
//	error: Any potential error that occurred during the database query.
func (accessControl *AccessControlRepo) IsPartnerPermissionExist(ctx context.Context, partnerID, permissionID string) (bool, error) {
	var exists bool

	query := `SELECT 1 FROM public.partner_permission WHERE partner_id = $1
	and permission_id=$2`
	err := accessControl.db.QueryRow(query, partnerID, permissionID).Scan(&exists)
	if err != nil {
		return false, err
	}

	return exists, nil
}

// check permission id is assigned to access_role_permission
func (accessControl *AccessControlRepo) HasAccessRolePermissionId(ctx context.Context, permissionID string) (bool, error) {
	var exists bool

	query := `SELECT 1 FROM public.access_role_permission WHERE permission_id = $1`
	err := accessControl.db.QueryRow(query, permissionID).Scan(&exists)
	if err != nil {
		return false, err
	}

	return exists, nil
}

func (accessControl *AccessControlRepo) DeletePermission(ctx context.Context, permissionID string) (int, error) {

	query := `DELETE FROM public.permission WHERE id = $1`
	result, err := accessControl.db.ExecContext(ctx, query, permissionID)
	if err != nil {
		return 0, err
	}

	// Check the affected rows
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(rowsAffected), nil
}

// check permission id is assigned to access_role_permission
func (accessControl *AccessControlRepo) IsPermissionExcists(ctx context.Context, permissionID string) (bool, error) {
	var exists bool

	query := `SELECT 1 FROM public.permission WHERE id = $1`
	err := accessControl.db.QueryRow(query, permissionID).Scan(&exists)
	if err != nil {
		return false, err
	}

	return exists, nil
}
