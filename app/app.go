package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"permission/config"
	"permission/internal/consts"
	"permission/internal/controllers"
	"permission/internal/entities"
	"permission/internal/repo"
	"permission/internal/repo/driver"
	"permission/internal/usecases"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"

	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
)

// Run method to run the service in gin framework
// env configuration
// logrus, zap
// use case intia
// repo initalization
// controller init
func Run() {
	// init the env config
	cfg, err := config.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	var log *logger.Logger

	// ################ For Logging to a File ################
	// Create a file logger configuration with specified settings.
	file := &logger.FileMode{
		LogfileName:  "access_control.log", // Log file name.
		LogPath:      "logs",               // Log file path. Add this folder to .gitignore as logs/
		LogMaxAge:    7,                    // Maximum log file age in days.
		LogMaxSize:   1024 * 1024 * 10,     // Maximum log file size (10 MB).
		LogMaxBackup: 5,                    // Maximum number of log file backups to keep.
	}

	// ################ For Logging to a Database ################
	// Create a database logger configuration with the specified URL and secret.
	// For development purpose don't use cloudMode.

	// ############# Client Options #############
	// Configure client options for the logger.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
		JSONFormater:        false,          // log format, if sets to false, default text formatter is used.
	}
	// Check if the application is in debug mode.
	if cfg.Debug {
		// Debug Mode: Logs will print to both the file and the console.

		// Initialize the logger with the specified configurations for file and console logging.
		log = logger.InitLogger(clientOpt, file)
	} else {
		// Release Mode: Logs will print to a database, file, and console.

		// Create a database logger configuration with the specified URL and secret.
		db := &logger.CloudMode{
			// Database API endpoint (for best practice, load this from an environment variable).
			URL: consts.LoggerServiceURL,
			// Secret for authentication.
			Secret: cfg.LoggerSecret,
		}

		// Initialize the logger with the specified configurations for database, file, and console logging.
		log = logger.InitLogger(clientOpt, db, file)
	}

	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect to the database")
		return
	}

	// // here initalizing the router
	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	api := router.Group("/api")

	//########### LogMiddleware will capture all the request scoped log data.
	api.Use(middleware.LogMiddleware(map[string]interface{}{}))

	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          "ERROR_CACHE_KEY_LABEL",
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", consts.LocalisationServiceURL),
		},
	))

	api.Use(middleware.EndpointExtraction(
		middleware.EndPointOptions{
			Cache:           cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration: time.Duration(time.Hour * 24),
			CacheKeyLabel:   "ENDPOINT_CACHE_KEY_LABEL",
			EndPointsURL:    fmt.Sprintf("%s/localization/endpointname", consts.EndpointURL),
		},
	))

	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))

	{

		{

			// repo initialization
			repo := repo.NewAccessControlRepo(pgsqlDB)

			// initilizing usecases
			useCases := usecases.NewAccessControlUseCases(repo)

			// initalizin controllers
			controller := controllers.NewAccessControlController(api, useCases)

			// init the routes
			controller.InitRoutes()
		}

	}

	// runn the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	fmt.Println("Server listening in...", cfg.Port)
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	// Use a separate channel for timeout signaling
	timeoutCh := make(chan struct{})

	go func() {
		time.Sleep(5 * time.Second)
		close(timeoutCh)
	}()

	select {
	case <-quit:
		log.Println("Shutdown Server ...")

		ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatal("Server Shutdown:", err)
		}

	case <-timeoutCh:
		log.Println("timeout of 5 seconds.")
		// Handle timeout logic
	}
}
